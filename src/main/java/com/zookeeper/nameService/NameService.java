package com.zookeeper.nameService;

import java.util.ArrayList;
import java.util.List;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.exception.ZkNodeExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 命名服务
 *
 */
public class NameService {
	
	 public static final Logger LOG = LoggerFactory.getLogger(NameService.class);
	
    //缓存时间  
    private static final int SESSION_TIME  = 2000;
    private static final String HOST  = "127.0.0.1:2181"; 
    
    protected ZkClient zkClient=null;
	
	private String nameroot = "/NameService";

	private String namerootvalue = "IsNameService";

	public NameService(){
		zkClient=new ZkClient(HOST, SESSION_TIME);
		try {
			if(zkClient!=null){
				if(!zkClient.exists(nameroot)){
					zkClient.createPersistent(nameroot,namerootvalue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 关闭ZK服务
	 */
	public void closeZk(){
		if (zkClient != null) {
			zkClient.close();
			System.out.println("zookeeper close success!");
		}
	}

	/**
	 * 待注册的名字字符串name，在zk中创建一个/NameService/name的znode路径
	 * 
	 * @param name
	 * @return
	 */
	public boolean registerService(String name) {
		String path = nameroot + "/" + name;
		boolean ret = false;
		try {
			if(!zkClient.exists(path)){
				zkClient.createPersistent(path);
				System.out.println(name + " registered success!");
				ret = true;
			}
		} catch (Exception e) {
			if(e instanceof ZkNodeExistsException){
				 System.out.println("连接创建失败，发生 ZkNodeExistsException , 节点已经存在 ");
			}else{
				System.out.println( "连接创建失败，发生 Exception , e " + e.getMessage());
			}
		}
		return ret;
	}
	
	public boolean deleteService(String name) {
		String path = nameroot + "/" + name;
		boolean ret = false;
		try {
			if(zkClient.exists(path)){
				zkClient.delete(path);
				System.out.println(name + " delete success!");
				ret = true;
			}
		} catch (Exception e) {
			if(e instanceof ZkNodeExistsException){
				System.out.println("删除失败，发生 ZkNodeExistsException , 节点已经存在 ");
			}else{
				System.out.println( "删除失败，发生 Exception , e " + e.getMessage());
			}
		}
		return ret;
	}
	
	
	public List<String> ReadAllService() {
		List<String> namelist = new ArrayList<String>();
		try {
			namelist = zkClient.getChildren(nameroot);
		} catch (Exception e) {
			System.out.println( "读取服务失败，发生 Exception , e " + e.getMessage());
		}
		return namelist;
	}
	
	public String cereateTempNode(String name,Object value){
		String path = nameroot + "/temp/" + name;
		try {
			//zkClient.createPersistent(path,true);
			String pathString = zkClient.createEphemeralSequential(path, value);
			System.out.println(name + " cereateTempNode success! path="
					+ pathString);
			return pathString;
		} catch (Exception e) {
			if(e instanceof ZkNodeExistsException){
				 System.out.println("连接创建失败，发生 ZkNodeExistsException , 节点已经存在 ");
			}else{
				System.out.println( "连接创建失败，发生 Exception , e " + e.getMessage());
			}
		}
		return "-----------------------";
	}
	
	public static void main(String[] args) {
		NameService nameService=new NameService();
		nameService.registerService("service_1");
		nameService.registerService("service_2");
		nameService.registerService("service_1");
		nameService.registerService("service_3");
		List<String> list=nameService.ReadAllService();
		System.out.println(list.toString());
		nameService.deleteService("service_1");
		
		for (int i = 0; i < 10; i++) {
			System.out.println(nameService.cereateTempNode("service_"+i,"123456_"+i));
		}
		
	}

}
